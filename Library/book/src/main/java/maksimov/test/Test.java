package maksimov.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        Configuration configure = configuration.configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session currentSession = sessionFactory.openSession();
        currentSession.beginTransaction();
    }
}
