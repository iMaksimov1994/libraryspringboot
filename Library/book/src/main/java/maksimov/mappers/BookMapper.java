package maksimov.mappers;


import maksimov.dto.BookDTO;
import maksimov.model.Book;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BookMapper {
    BookDTO toBookDTO(Book book);

    Book fromBookDTO(BookDTO bookDTO);
}

