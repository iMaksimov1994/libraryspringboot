package maksimov.mappers;


import maksimov.dto.AuthorDTO;
import maksimov.model.Author;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AuthorMapper {
    AuthorDTO toAuthorDTO(Author author);

    Author fromAuthorDTO(AuthorDTO authorDTO);
}
