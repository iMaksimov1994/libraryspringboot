package maksimov.exceptions;

import org.springframework.http.HttpStatus;

public class EntityNotExistOrNotFound extends BaseException {
    public EntityNotExistOrNotFound(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }
}
