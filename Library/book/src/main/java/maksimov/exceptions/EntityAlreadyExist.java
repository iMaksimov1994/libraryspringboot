package maksimov.exceptions;

import org.springframework.http.HttpStatus;

public class EntityAlreadyExist extends BaseException {

    public EntityAlreadyExist(String message) {
        super(message, HttpStatus.CONFLICT);
    }
}
