package maksimov.service;


import maksimov.dto.BookDTO;

public interface BookService {
    BookDTO addBook(BookDTO bookDTO);

    BookDTO getQuantityBookByTittle(String tittle);
}
