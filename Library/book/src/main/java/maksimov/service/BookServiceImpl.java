package maksimov.service;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import maksimov.dto.AuthorDTO;
import maksimov.dto.BookDTO;
import maksimov.exceptions.EntityAlreadyExist;
import maksimov.exceptions.EntityNotExistOrNotFound;
import maksimov.mappers.AuthorMapper;
import maksimov.mappers.BookMapper;
import maksimov.model.Author;
import maksimov.model.Book;
import maksimov.repository.AuthorRepository;
import maksimov.repository.BookRepository;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final BookMapper bookMapper;
    private final AuthorMapper authorMapper;

    @Override
    @Transactional()
    @Retryable(maxAttempts = 2)
    public BookDTO addBook(BookDTO bookDTO) {
        Optional<Book> bookByTittleIgnoreCase = bookRepository.getBookByTittleIgnoreCase(bookDTO.getTittle());
        if (bookByTittleIgnoreCase.isPresent()) {
            log.info("Book already exist!");
            throw new EntityAlreadyExist("Book already exist!");
        } else {
            List<AuthorDTO> authorDTOs = bookDTO.getAuthors();
            List<Author> authors = authorDTOs.stream().map(authorMapper::fromAuthorDTO).toList();

            List<Author> authorsInDB = new ArrayList<>(authors.stream().map(x ->
                    this.authorRepository.findAuthorByNameAndLastNameIgnoreCase(x.getName(),
                            x.getLastName())).filter(Optional::isPresent).map(Optional::get).toList());

            List<Author> originalAuthors = authors.stream().filter(x ->
                            this.authorRepository.findAuthorByNameAndLastNameIgnoreCase(x.getName(),
                                    x.getLastName()).isEmpty()).
                    toList();
            authorsInDB.addAll(originalAuthors);
            Book book = bookMapper.fromBookDTO(bookDTO);
            book.setAuthors(authorsInDB);


            Book saveBook = this.bookRepository.save(book);
            log.info("Save book in database correctly:{}", saveBook);
            return bookMapper.toBookDTO(saveBook);
        }
    }

    @Override
    public BookDTO getQuantityBookByTittle(String tittle) {
        Book book = bookRepository.getBookByTittleIgnoreCase(tittle).
                orElseThrow(() -> new EntityNotExistOrNotFound("Book not found by this tittle!"));
        log.info("Get book from database correctly by tittle:{}", book);
        int quantity = book.getQuantity();
        log.info("Get quantity of books with the same name:{}", quantity);
        return bookMapper.toBookDTO(book);
    }
}
