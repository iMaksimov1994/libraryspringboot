package maksimov.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "author",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "last_name"})},
        indexes = {@Index(name = "fio_idx", columnList = "name,last_name")})
@Data
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "last_name", nullable = false)
    private String lastName;
    @ToString.Exclude
    @ManyToMany(mappedBy = "authors")
    private List<Book> books = new ArrayList<>();
}
