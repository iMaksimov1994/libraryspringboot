package com.maksimov;


import com.maksimov.dto.BookDtoResponse;
import com.maksimov.feign.BookClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;


@SpringBootTest
public class ResponseEnityTest {
    @Autowired
    BookClient client;

    @Test
    public void entityTest() {
        ResponseEntity<BookDtoResponse> book = client.getBook("naruto");
        System.out.println(book);
    }

}
