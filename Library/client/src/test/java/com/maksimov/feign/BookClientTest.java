package com.maksimov.feign;

import com.maksimov.dto.AuthorDTO;
import com.maksimov.dto.BookDtoRequest;
import com.maksimov.dto.BookDtoResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.*;

@SpringBootTest
class BookClientTest {
    @Autowired
    BookClient client;
    final long elements = 1000;
    ExecutorService executorService = Executors.newFixedThreadPool(15);
    Random rand = new Random();


    @Test
    void testBooks() throws InterruptedException {
        long start = System.currentTimeMillis();
        Map<String, BookDtoRequest> map = generateBookAndAuthor();
        createBooks(map);
        getBooks(map);
        //deleteBooks(map);
        System.out.println(System.currentTimeMillis() - start + " millis to all operations");
    }

    void createBooks(Map<String, BookDtoRequest> map) throws InterruptedException {
        long start = System.currentTimeMillis();

        List<CreateBook> tasks = map.values().stream().map(CreateBook::new).toList();
        List<Future<ResponseEntity<BookDtoResponse>>> futuresBooks = executorService.invokeAll(tasks);
        futuresBooks.parallelStream().forEach(book -> {
            try {
                if (!book.get().getStatusCode().is2xxSuccessful()) {
                    System.out.println(book.get().getStatusCode());
                }
            } catch (InterruptedException e) {
                System.out.println("Ошибка");
                System.out.println(e);
            } catch (ExecutionException e) {
                System.out.println("Ошибка");
                System.out.println(e);
            }
        });

        System.out.println(System.currentTimeMillis() - start + "millis to create all books");
    }

    void getBooks(Map<String, BookDtoRequest> map) throws InterruptedException {
        long start = System.currentTimeMillis();

        List<GetBook> tasks = map.keySet().stream().map(GetBook::new).toList();
        List<Future<ResponseEntity<BookDtoResponse>>> futuresBooks = executorService.invokeAll(tasks);
        futuresBooks.parallelStream().forEach(book -> {
            try {
                System.out.println(book.get().getStatusCode());
            } catch (InterruptedException e) {
                System.out.println("Ошибка");
                System.out.println(e);
            } catch (ExecutionException e) {
                System.out.println("Ошибка");
                System.out.println(e);
            }
        });

        System.out.println(System.currentTimeMillis() - start + "millis to get all books");
    }

    void deleteBooks(Map<String, BookDtoRequest> map) throws InterruptedException {
        long start = System.currentTimeMillis();

        List<DeleteBook> tasks = map.keySet().stream().map(DeleteBook::new).toList();
        List<Future<ResponseEntity<Void>>> futuresBooks = executorService.invokeAll(tasks);
        futuresBooks.parallelStream().forEach(response -> {
            try {
                System.out.println(response.get().getStatusCode());
            } catch (InterruptedException e) {
                System.out.println("Ошибка");
            } catch (ExecutionException e) {
                System.out.println("Ошибка");
            }
        });

        System.out.println(System.currentTimeMillis() - start + "millis to delete all books");
    }

    private Map<String, BookDtoRequest> generateBookAndAuthor() {
        Map<String, BookDtoRequest> booksAndAuthors = new HashMap<>();
        for (int i = 0; i < elements; i++) {
            BookDtoRequest book = new BookDtoRequest();
            book.setTittle("Book#" + i);
            book.setQuantity(rand.nextLong(1, 10));
            int number1 = rand.nextInt(1, 300);
            int number2 = rand.nextInt(301, 600);
            int number3 = rand.nextInt(601, 900);
            book.setAuthors(List.of(new AuthorDTO("Author#" + number1, "Lastname#" + number1),
                    new AuthorDTO("Author#" + number2, "Lastname#" + number2),
                    new AuthorDTO("Author#" + number3, "Lastname#" + number3)));
            booksAndAuthors.put(book.getTittle(), book);
        }
        return booksAndAuthors;
    }

    public class CreateBook implements Callable<ResponseEntity<BookDtoResponse>> {
        private BookDtoRequest bookDtoRequest;

        public CreateBook(BookDtoRequest bookDtoRequest) {
            this.bookDtoRequest = bookDtoRequest;
        }

        @Override
        public ResponseEntity<BookDtoResponse> call() throws Exception {
            return client.createBook(bookDtoRequest);
        }
    }

    public class GetBook implements Callable<ResponseEntity<BookDtoResponse>> {
        private String title;

        public GetBook(String title) {
            this.title = title;
        }

        @Override
        public ResponseEntity<BookDtoResponse> call() throws Exception {
            return client.getBook(title);
        }
    }

    public class DeleteBook implements Callable<ResponseEntity<Void>> {
        private String title;

        public DeleteBook(String title) {
            this.title = title;
        }

        @Override
        public ResponseEntity<Void> call() throws Exception {
            return client.removeBook(title);
        }
    }
}