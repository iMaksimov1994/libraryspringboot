package com.maksimov;


import com.maksimov.dto.BookDtoResponse;
import com.maksimov.feign.BookClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;


@EnableFeignClients
@SpringBootApplication
public class HttpClientApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(HttpClientApplication.class, args);
        BookClient client = context.getBean(BookClient.class);
        ResponseEntity<BookDtoResponse> book = client.getBook("test");
        System.out.println(book);
    }

}
