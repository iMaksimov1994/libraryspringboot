package com.maksimov.feign;


import com.maksimov.dto.BookDtoRequest;
import com.maksimov.dto.BookDtoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "book-client", url = "${client.url}")
public interface BookClient {
    @GetMapping("/books/{title}")
    ResponseEntity<BookDtoResponse> getBook(@PathVariable String title);

    @PostMapping("/books")
    ResponseEntity<BookDtoResponse> createBook(@RequestBody BookDtoRequest bookDtoRequest);

    @DeleteMapping("/books/{title}")
    ResponseEntity<Void> removeBook(@PathVariable String title);
}
