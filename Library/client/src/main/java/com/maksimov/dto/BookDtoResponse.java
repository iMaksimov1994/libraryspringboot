package com.maksimov.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class BookDtoResponse {
    private Long id;
    private String title;
    private Long quantity;
    private Set<AuthorDtoResponse> authors;
}
