package com.maksimov.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthorDtoResponse {
    private Long id;
    private String name;
}

