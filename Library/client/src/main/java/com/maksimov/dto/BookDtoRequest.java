package com.maksimov.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookDtoRequest {
    private String tittle;
    private Long quantity;
    private List<AuthorDTO> authors;
}